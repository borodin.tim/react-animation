import React, { Component } from "react";
import { Transition, CSSTransition } from 'react-transition-group';

import "./App.css";
import Modal from "./components/Modal/Modal";
import CSSModal from "./components/Modal/CSSModal";
import Backdrop from "./components/Backdrop/Backdrop";
import List from "./components/List/List";

class App extends Component {
  state = {
    modalIsOpen: false,
    showBlock: false
  }

  showModal = () => {
    this.setState({ modalIsOpen: true });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  render() {
    return (
      <div className="App">
        <h1>React Animations</h1>

        <button
          className="Button"
          onClick={() => this.setState(prevState => ({
            showBlock: !prevState.showBlock
          }))}
        >
          Toggle
        </button>
        <br />
        <Transition
          in={this.state.showBlock}
          timeout={500}
          mountOnEnter
          unmountOnExit
          onEnter={() => {
            console.log('onEnter')
          }}
          onEntering={() => {
            console.log('onEntering')
          }}
          onEntered={() => {
            console.log('onEntered')
          }}
          onExit={() => {
            console.log('onExit')
          }}
          onExiting={() => {
            console.log('onExiting')
          }}
          onExited={() => {
            console.log('onExited')
          }}
        >
          {state => (<div style={{
            backgroundColor: 'red',
            width: 100,
            height: 100,
            margin: 'auto',
            transition: 'opacity 0.5s ease-in-out',
            opacity: state === 'entering' ? 0 : state === 'entered' ? 1 : state === 'exiting' ? 0 : 0
          }} >{state}</div>
          )
          }
        </Transition>

        <br />

        {/* <Modal show={this.state.modalIsOpen} closed={this.closeModal} /> */}
        {
          /* this.state.modalIsOpen &&
          <Backdrop show={this.state.modalIsOpen} /> */
        }
        {/* <button
          className="Button"
          onClick={this.showModal}
        >
          Open Modal
        </button> */}

        <CSSModal show={this.state.modalIsOpen} closed={this.closeModal} />
        {
          this.state.modalIsOpen &&
          <Backdrop show={this.state.modalIsOpen} />
        }
        <button
          className="Button"
          onClick={this.showModal}
        >
          Open CSSModal
        </button>
        <h3>Animating Lists</h3>
        <List />
      </div>
    );
  }
}

export default App;
